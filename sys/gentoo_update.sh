#!/usr/env/bin bash

LOG_FILE="/var/log/emerge-update.log"
ERROR_FILE="/var/log/emerge-error.log"

# Sync and update
emerge --sync
if emerge --pretend --update --deep --newuse @world | grep -q '[ebuild]'; then
    echo "$(date '+%d-%b_%Y') - Updates available. Proceeding..." >> $LOG_FILE
    emerge --update --deep --newuse @world >> $LOG_FILE 2>> $ERROR_FILE
    if [ $? -ne 0 ]; then
        echo "$(date '+%d-%b_%Y') - Error during update. Manual intervention required." | mail -s "Gentoo Update Error" senoraraton@gmail.com
    else
        echo "$(date '+%d-%b_%Y') - Update successful. Running depclean and revdep-rebuild." >> $LOG_FILE
        emerge --depclean >> $LOG_FILE
        revdep-rebuild >> $LOG_FILE
    fi
else
    echo "$(date '+%d-%b_%Y') - No updates available." >> $LOG_FILE
fi


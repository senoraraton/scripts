# https://www.mit.edu/~arosinol/2019/08/01/Simplifying_build_commands__cmake___make___cbuild/

#!/usr/bin/env python

import sys
import os
import subprocess
import argparse

# ANSI color codes for terminal output
RED = '\033[31m'
GREEN = '\033[32m'
RESET = '\033[0m'

def cmake():
    """Run cmake command to generate build files."""
    print(GREEN + "Calling cmake:" + RESET)
    if not os.path.exists('./CMakeLists.txt'):
        raise Exception('Missing CMakeLists.txt.')
    try:
        subprocess.call('cmake ..', shell=True, cwd="./build")
    except:
        raise Exception(RED + 'cmake failed.' + RESET)
    return True

def make_clean():
    """Run make clean command."""
    print(GREEN + "Calling make clean." + RESET)
    try:
        subprocess.call('make clean', shell=True, cwd='./build')
    except:
        raise Exception(RED + 'make clean failed.' + RESET)
    return True

def make_test():
    """Run make test command."""
    print(GREEN + "Calling make test." + RESET)
    try:
        subprocess.call('make test', shell=True, cwd='./build')
    except:
        raise Exception(RED + 'make test failed.' + RESET)
    return True

def make():
    """Run make command to build the project."""
    print(GREEN + "Calling make:" + RESET)
    try:
        subprocess.call('make -j $(nproc)', shell=True, cwd='./build')
    except:
        raise Exception(RED + 'make failed.' + RESET)
    return True

def run():
    """Run the compiled binary."""
    print(GREEN + "Running the compiled binary:" + RESET)
    binary_path = os.path.join('./build', 'App')  # Adjust 'App' to your actual binary name
    if not os.path.exists(binary_path):
        raise Exception(RED + 'Binary not found. Run `make` first.' + RESET)
    try:
        subprocess.call('./App', shell=True, cwd='./build')
    except:
        raise Exception(RED + 'Failed to run the binary.' + RESET)
    return True

def parser():
    """Create argument parser."""
    parser = argparse.ArgumentParser(add_help=True,
                                     description="Automated build and run script using cmake + make.")
    parser.add_argument("-c", "--clean", action="store_true", help="Call make clean.")
    parser.add_argument("-t", "--test", action="store_true", help="Call make test.")
    parser.add_argument("-r", "--run", action="store_true", help="Run the compiled binary.")
    return parser

if __name__ == "__main__":
    # Parse command line arguments
    args = parser().parse_args()

    # Ensure build directory exists
    if not os.path.isdir('build'):
        print(GREEN + 'Creating build folder.' + RESET)
        os.mkdir('build')

    # Execute cmake to generate build files
    assert cmake()

    # Clean target
    if args.clean:
        assert make_clean()

    # Build target
    assert make()

    # Test target
    if args.test:
        assert make_test()

    # Run target
    if args.run:
        assert run()
